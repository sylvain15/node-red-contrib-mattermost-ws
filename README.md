# node-red-contrib-mm
Interface with mattermost, based on initial node-red-contrib-mattermost, with small modifications to allow direct access to all methods exposed in mattermost-client lib.
This module requires to create a bot account on mattermost, and uses websocket for connection.

Creates 5 nodes: 
- mm-config: configuration of the bot
- mm-receive: triggered whenever a message is received by the bot
- mm-send: send a message to users (dm, use "channel=username") or to groups
- mm-upload: uploads files from the file-system, that can be attached to a message
- mm-api: use mattermost API to perform more operations


# set up a token

To set up a token, go to Mattermost integrations and create a bot-account. Fill the received data under  and `Personal Access Token`.
- In Mattermost go to integrations -> select bot accounts
- create a bot account
- click "add bot account" at the top, and fill in all requested data. Enable `post:all` to allow the bot to send and receive(!!) to channels.
- Create New Token. Give it any name, e.g. a number or date
- Copy `Token ID` to `Username / Token ID`, and `Access Token` to `Personal Access Token` into the Node-Red config. Keep a copy of that access token in a safe place!

# Examples
Below are some examples for what data needs to be provided.

## mm-receive
Any time a message arrives on a channel that is being configured, you get messages like this:
```json
{
    payload: {
        "event": "posted",
        "data": object,
        "channel_display_name": "",
        "channel_name": "ooru7g666f8wmm99dbe9osmeno__qz93pd58ifnpzj84f3pxg859eh",
        "channel_type": "D",
        "mentions": "["qz93pd58ifnpzj84f3pxg859eh"]",
        "post": object,
        "sender_name": "benoit",
        "team_id": "",
        "broadcast": object,
        "seq": 6,
    },
    _msgid: "8919fda8.df6f1"
}
```
You need to experiment to see what data is available for your case.

## mm-send
Sends a message to a channel.

Example:
```json
{
    "channel": "user",
    "payload": {
        "message": "Test",
        //root_id: rootMsgId,
        //parent_id: originalMsgId,
        //original_id: ""
    }
}
```
There is a maximum of 4000 characters per message. If longer, it will be cut in parts. If you want nice cuts, you have to do that yourself.

## mm-upload
Uploads a file as link to local file or defined by Buffer.

Example:
```json
{
    "channel": "user",
    "payload": ["/tmp/report.html"]
}
```
Connect it to a `mm-send` node (with a message as payload) to make sure the files gets visible in Mattermost. Without a message, the attachment will be lost.

## mm-api
Currently not working as intended - see issue #11 
Makes a direct call to a function in [mattermost-client](https://github.com/loafoe/mattermost-client). Below are the options:

### teams
- getTeams()

### users
- loadUsers(page = 0)
- loadUser(user_id)
- getUserByID(id)
- getUserByEmail(email)

### channels
- loadChannels()
- getAllChannels() 
- getChannelByID(id)
- findChannelByName(name)
- setChannelHeader(channelID, header)

### messages
- customMessage(postData, channelID)
- dialog(trigger_id, url, dialog)
- editPost(post_id, msg)
- react(messageID, emoji)
- unreact(messageID, emoji)
- postMessage(msg, channelID)
- postCommand(channelID, cmd)

Example :
```json
{
    "method": "getChannelByID",
    "args": "SomeID",
};
```
